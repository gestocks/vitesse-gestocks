/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  theme: {
    colors: {
      ens: { DEFAULT: '#00778B', dark: '#003B45', light: '#7fbbc5' },
      jauneens: { DEFAULT: '#FFB432', dark: '#7f5a19', light: '#ffd998' },
      blueens: { DEFAULT: '#508CD2', dark: '#284669', light: '#a7c5e8' },
      maronens: { DEFAULT: '#D2A05A', dark: '#69502d', light: '#E8CFAC' },
      noirens: { DEFAULT: '#000000' },
      vertens: { DEFAULT: '#82BE32', dark: '#415f19', light: '#c0de98' },
      grisens: { DEFAULT: '#BEBEBE', dark: '#5f5f5f', light: '#dedede' },
      orangeens: { DEFAULT: '#F0825A', dark: '#78412d', light: '#f7c0ac' },
    },
    extend: {},
  },
  plugins: [],
}
